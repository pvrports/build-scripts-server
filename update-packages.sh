#!/bin/bash

#
# PVRPORTS AUTOMATED BUILD SCRIPT FOR SERVERS
# TESTED UNDER ALPINE LINUX 3.16
#
# The MIT License (MIT)
# Copyright © 2022 Antoni Aloy Torrens <aaloytorrens@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# https://mit-license.org/
#

# KEYPAIR MUST BE CREATED BEFORE RUNNING THIS FILE!
# `pmbootstrap init` MUST BE EXECUTED ONCE BEFORE RUNNING THIS FILE!
# To create a keypair:
# apk add gcc abuild --no-cache
# abuild-keygen -a -i

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. You must provide the ABSOLUTE path of your apk key."
    exit 0
fi

# Tweak PMOS_VERSION as you wish
# TODO: Provide PMOS_VERSION as argument
REPO_NAME="pvrports"
PACKAGES_LOCATION=/home/$USER/$REPO_NAME/postmarketos
REPOS_LOCATION=/home/$USER/.local/var
PMOS_VERSION="v21.12"
PVRPORTS_VERSION=$PMOS_VERSION
declare -a PMOS_VERSIONS_HISTORY=("v21.12" "v22.06" )

# 0. SETUP

# - Remove buildroots
pmbootstrap -y zap

# - Recreate pmaports just in case something went wrong
rm -rf $REPOS_LOCATION/pmbootstrap/cache_git/pmaports/

# - And clone it again:
yes "" | pmbootstrap init

# - Switch to stable branch:
git -C $REPOS_LOCATION/pmbootstrap/cache_git/pmaports checkout $PMOS_VERSION

# - Remove conflicting packages
rm -rf $REPOS_LOCATION/pmbootstrap/cache_git/pmaports/device/community/*samsung-espresso3g*
rm -rf $REPOS_LOCATION/pmbootstrap/cache_git/pmaports/non-free/sgx-ddk-um

# - Create PACKAGES_LOCATION
mkdir -pv $PACKAGES_LOCATION

# - Install packages
sudo apk add abuild grep rsync coreutils findutils python3 py3-pip

# - Upgrade pmbootstrap
sudo pip3 install pmbootstrap -U

# 0. Clone pvrports repo

git clone $REPOS_LOCATION/pvrports

# 1. Checkout pvrports to the correct branch (v22.06 is master, no need checkout)

if [ "${PVRPORTS_VERSION}" != "v22.06" ]; then
	git -C $REPOS_LOCATION/pvrports checkout $PVRPORTS_VERSION
fi

# 2. Update pvrports repo
git -C $REPOS_LOCATION/pvrports pull origin $PVRPORTS_VERSION

# 3. Update pmaports repo
pmbootstrap pull

# Workaround
git -C $REPOS_LOCATION/pmbootstrap/cache_git/pmaports pull

# 4. Build packages for all architectures

for CATEGORY in main community testing non-free; do

    # 5. Copy packages to pmaports/main
    cp -vr $REPOS_LOCATION/$REPO_NAME/"$CATEGORY"/* $REPOS_LOCATION/pmbootstrap/cache_git/pmaports/main/

    # 6. Build packages for each architecture
    # TODO: Skip the package if there's an existing .apk file already built
     for apkbuild in $(find $REPOS_LOCATION/$REPO_NAME/"$CATEGORY"/* -type d -printf "%f\n"); do
     #for apkbuild in $(find ~/.local/var/pmbootstrap/cache_git/pmaports/main/hello-world -type d -printf "%f\n"); do # SAMPLE FOR DEBUG

		# 6.1. Read architecture for apkbuild
		archs=$(grep -hr "^arch=" $REPOS_LOCATION/$REPO_NAME/"$CATEGORY"/$apkbuild)
		#archs=$(grep -hr "^arch=" ~/.local/var/pmbootstrap/cache_git/pmaports/main/hello-world) # SAMPLE FOR DEBUG
		archs=${archs/\"/}
		archs=${archs/arch\=}
		archs=${archs::-1} # Remove last " on the end of the string

		# 6.2. Read pkgver for apkbuild
		pkgver=$(grep -hr "^pkgver" $REPOS_LOCATION/$REPO_NAME/"$CATEGORY"/$apkbuild)
		pkgver=${pkgver/\"/}
		pkgver=${pkgver/pkgver\=}

		# 6.3. Read pkgrel for apkbuild
		pkgrel=$(grep -hr "^pkgrel" $REPOS_LOCATION/$REPO_NAME/"$CATEGORY"/$apkbuild)
                pkgrel=${pkgrel/\"/}
                pkgrel=${pkgrel/pkgrel\=}

		echo "apkbuild: $apkbuild; archs: $archs; pkgver: $pkgver, pkgrel: $pkgrel"

		# Ignore error "Too many arguments", it's okay
		# Ex: archs="aarch64 armv7 armhf" -> Too many arguments
		# Ex: archs="all" -> The conditional is executed
		echo "Ignore the following error (if any): \"Too many arguments\""
		if [ $archs == "all" ] || [ $archs == "noarch" ]; then
		    archs="x86_64 x86 aarch64 armv7 armhf"
		fi

		# 6.2. Split archs by spaces, see https://stackoverflow.com/a/1975873
		list_arch=($archs) 			# FIXME: This can only be executed with BASH shell.
		for archx in ${list_arch[@]}; do 	# FIXME: This can only be executed with BASH shell.
			# Skip armhf builds. Missing gcc-armhf.
			if [ "${archx}" != "armhf" ] && ([ "${PMOS_VERSION}" == "v22.06" ] || [ "${PMOS_VERSION}" == "v21.12" ]); then
				echo "Checking location: $REPOS_LOCATION/pmbootstrap/packages/$PMOS_VERSION/$archx/$apkbuild-$pkgver-r$pkgrel.apk"
				if [ -f "$REPOS_LOCATION/pmbootstrap/packages/$PMOS_VERSION/$archx/$apkbuild-$pkgver-r$pkgrel.apk" ]; then
					echo "Skipping $apkbuild, as apkbuild-$pkgver-r$pkgrel.apk already exists on $archx."
					echo "Skipping $apkbuild, as apkbuild-$pkgver-r$pkgrel.apk already exists on $archx." >> $HOME/.local/log.txt
				else
					echo "Building $apkbuild for arch $archx :"
					pmbootstrap build --force --arch="$archx" "$apkbuild" || echo "$(date +"%m-%d-%Y, %H:%M"): Failed to build $apkbuild in $archx" >> $HOME/.local/log.txt;
					pmbootstrap -y zap
				fi
			fi
		done
    done

    # 7. Remove my packages from pmaports to make CLEAN status in pmbootstrap status
    for apkbuild in $(find $REPOS_LOCATION/$REPO_NAME/"$CATEGORY"/* -type d -printf "%f\n"); do
        rm -rvf $REPOS_LOCATION/pmbootstrap/cache_git/pmaports/main/"$apkbuild";
    done
done

# 8. Copy pmaports packages to alpine_packages
mkdir -pv $PACKAGES_LOCATION/
sudo cp -vr $REPOS_LOCATION/pmbootstrap/packages/* $PACKAGES_LOCATION/

# 9. Setting up the repository and signing the APKINDEX
echo "Signing the repository index..."
for pmos_ver in ${PMOS_VERSIONS_HISTORY[@]}; do		# FIXME: This can only be executed with BASH shell.
	echo "pmos_ver: $pmos_ver"
	for architecture in x86_64 x86 aarch64 armv7; do
		echo "architecture: $architecture"
		sudo apk index -vU -o $PACKAGES_LOCATION/$pmos_ver/$architecture/APKINDEX.tar.gz $PACKAGES_LOCATION/$pmos_ver/$architecture/*.apk
		sudo abuild-sign -k $1 $PACKAGES_LOCATION/$pmos_ver/$architecture/APKINDEX.tar.gz
	done
done

# 11. Set permissions on Alpine Packages
sudo chmod -R 755 $PACKAGES_LOCATION/
sudo chown -R $USER:$USER $PACKAGES_LOCATION/

# 12. Copy pvrports packages to /var/www/pvrports
rm -rf $PACKAGES_LOCATION/$PMOS_VERSION/main
rm -rf $PACKAGES_LOCATION/$PMOS_VERSION/community
rm -rf $PACKAGES_LOCATION/$PMOS_VERSION/testing
rm -rf $PACKAGES_LOCATION/$PMOS_VERSION/non-free
rm -rf $PACKAGES_LOCATION/$PMOS_VERSION/device
sudo rm -rf /var/www/pvrports/pvrports/*
sudo mkdir -pv /var/www/pvrports/pvrports
sudo cp -vr $PACKAGES_LOCATION/* /var/www/pvrports/pvrports

# 13. Remove old location
rm -rf $PACKAGES_LOCATION/

# 14. Create symlink
cd /var/www/pvrports/pvrports/$PMOS_VERSION
sudo ln -s ../$PMOS_VERSION pvrports

# 15. Set ownership
sudo chown -R nginx:nginx /var/www/pvrports/pvrports
